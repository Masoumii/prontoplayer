<?php
class DatabaseConnection
{
    
    private static $db;
    
    //voorkom meerdere "instances"
    private function __construct(){}
    
    // Static methode om connectie functie te krijgen
    public static function getConnection()
    {
        
        //Als er geen connectie objecten zijn, maak dan een nieuwe
        if (!self::$db) :
            
            // Nieuwe connectie object
            self::$db = self::openNewConnection();
		endif;
        
        //return de connectie
        return self::$db;
    }
    
    // connectie openen
    private static function openNewConnection()
    {
        
        $connection = null;
        
        try {
            // Database gegevens vereist om in te loggen
            $connection = new PDO('mysql:host=localhost;port=3306;dbname=u18883p15056_arad', 'u18883p15056_arad', 'azlac270391');
            
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            
            // Fouten opvangen
        }
        catch (PDOException $e) {
            
            echo "Fout met verbinden: " . $e->getMessage();
        }
        
        return $connection;
    }
    
}
?>