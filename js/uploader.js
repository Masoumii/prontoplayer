function _(el){
    return document.getElementById(el);
}

function uploadFile(){

        //Disable form(empty playlist) button when uploading
        $(".fixed-nav-bar-bottom").prop('disabled', true);
        // Only if user has selected a file, Start upload progess
        if(document.getElementById("audio_file").value != "") {
    
    var file = _("audio_file").files[0];
    var formdata = new FormData();
    formdata.append("audio_file", file);
    
    // Upload file with AJAX
    var ajax = new XMLHttpRequest();
    ajax.upload.addEventListener("progress", progressHandler, false);
    ajax.addEventListener("load", completeHandler, false);
    ajax.addEventListener("error", errorHandler, false);
    ajax.addEventListener("abort", abortHandler, false);
    ajax.open("POST", "file_upload_parser.php");
    ajax.send(formdata);

            // If no file selected, show user a notification and do not upload anything.
        }else{
            $(".fixed-nav-bar-bottom").val("Geen geluidsbestand gekozen...");
    }
}

function progressHandler(event){

    // Hide upload section when upload is busy
    $("#audio_file, #hide-btn").fadeOut();
    var percent = (event.loaded / event.total) * 100;
    
    //Notification bar to show upload progress
    $(".fixed-nav-bar-bottom").val(Math.round(percent)+"% geupload");
    
    // If 100% reached, then show notification that upload is wrapping up.
    if(Math.round(percent) == 100){
        $(".fixed-nav-bar-bottom").val("Aan het afronden...");
    }

    // Increase navbar width based on upload percentage
    $(".fixed-nav-bar-bottom").css("width", Math.round(percent)+"%");

}

function completeHandler(event){

    // Hide previous notification bar, slide down new one to tell user that upload is finished
    $(".fixed-nav-bar-bottom").val("Geupload naar playlist!");

     // Slide up the notification bar and hard-reload page
     setTimeout(function(){
        window.location.reload(true); 
        }, 3000);
    
}

function errorHandler(event){

    // Show notification if upload failed
    $(".fixed-nav-bar-bottom").val("Uploaden is niet gelukt...");
}
    // Show notification if upload canceled
function abortHandler(event){
    $(".fixed-nav-bar-bottom").val("Uploaden is geannuleerd...");
}