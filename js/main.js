jQuery(document).ready(function() {

    // Slide down playlist on page-load
    $('.playlist').slideDown(1000);

    function showNotif(){
        $(".fixed-nav-bar").hide();
        $(".fixed-nav-bar").slideDown(1000);
    }
 
    // inner variables
    var song;
    var tracker = $('.tracker');
    var volume = $('.volume');
    var showTime = $('.currTime');
    var curtime;

    // Confirm before emptying playlist
    document.getElementsByName('empty-playlist')[0].onsubmit = function() {
        return confirm('Weet je zeker dat je alles wilt verwijderen?');
    };

    // Initialize the song
    function initAudio(elem) {

        var url = elem.attr('audiourl');
        var title = elem.text();

        // Show the title of the song on the notifcation bar
        $('.fixed-nav-bar').html('<img width="20px" src="images/now-playing.png">'+'&nbsp;&nbsp;'+title);

        song = new Audio('data/music/' + url);

        // timeupdate event listener
        song.addEventListener('timeupdate',function (){


            // autoplay Next Song - if song ended function
            song.onended = function() {

                // Wait 3 seconds then choose next song in the list, initialize and play it.
                setTimeout(function(){
                    
                    stopAudio();
                    var next = $('.playlist li.active').next();

                    if (next.length == 0) {
                        next = $('.playlist li:first-child');
                    }

                    $('.playlist li').removeClass('active');
            
                    initAudio(next);
                    playAudio();
                     }, 2000);
            };

                    // empty tracker slider
            tracker.slider({
                range: 'min',
                min: 0,
                max: song.duration,
                value:100,
                start: function(event, ui) {},
                slide: function(event, ui) {
                    song.currentTime = ui.value;
                },
                stop: function(event, ui) {}
            });

            var curtime = parseInt(song.currentTime, 10);
            tracker.slider('value', curtime);

            // String to time for song duration while playing
            hours = Math.floor(curtime / 3600);
            curtime %= 3600;
            minutes = Math.floor(curtime / 60);
            seconds = curtime % 60;

            // Add a extra zero to the time for better readability
            if(seconds<10){
                seconds = "0"+seconds;
            }
            if(minutes<10){
                minutes = "0"+minutes;
            }
            if(hours<10){
                hours = "0"+hours;
            }

            // Display the current song and its current time
            $('.fixed-nav-bar').text(title+" - "+minutes+":"+seconds);

            // Update the page title to the song-title and its current time
            $(document).attr("title", title+" - "+minutes+":"+seconds);  

        });

        $('.playlist li').removeClass('active');
        elem.addClass('active');
    }

    // Play audio
    function playAudio() {

        showNotif();   
        song.volume = $('.ui-slider-range').width() / $('.ui-slider').width();
        song.play();
        $('.play').addClass('hidden');
        $('.pause').addClass('visible');
    }

    // Stop audio
    function stopAudio() {
        song.pause();
        $('.play').removeClass('hidden');
        $('.pause').removeClass('visible');
    }

    // play click
    $('.play').click(function (e) {
        e.preventDefault();
        playAudio();
    });

    // pause click
    $('.pause').click(function (e) {
        e.preventDefault();
        stopAudio();
    });

    // forward click
    $('.fwd').click(function (e) {
        e.preventDefault();
        stopAudio();

        var next = $('.playlist li.active').next();
        if (next.length == 0) {
            next = $('.playlist li:first-child');
        }

        initAudio(next);
        playAudio();
    });

    // rewind click
    $('.rew').click(function (e) {
        e.preventDefault();
        stopAudio();

        var prev = $('.playlist li.active').prev();
        if (prev.length == 0) {
            prev = $('.playlist li:last-child');
        }
        initAudio(prev);
        playAudio();
    });

    // playlist elements - click
    $('.playlist li').click(function (e) {
        e.preventDefault();
        stopAudio();
        initAudio($(this));        
        playAudio();
    });

    // initialization - first element in playlist
    initAudio($('.playlist li:first-child'));

    // initialize the volume slider
    volume.slider({
        range: 'min',
        min: 1,
        max: 100,
        value:100,
        start: function(event, ui) {},
        slide: function(event, ui) {
            song.volume = ui.value / 100;
        },
        stop: function(event,ui) {},
    });

  
});