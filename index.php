<?php
require "class.mp3.php";
$player = new MP3();
?>

<!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="utf-8" />
    <meta name="author" content="Script Tutorials" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <title></title>
    <link href="css/styles.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="css/normalize.css" />
    <link rel="stylesheet" type="text/css" href="css/component.css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.8.21.custom.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
    <script type="text/javascript" src="js/remover.js"></script>
    
         

<!-- Script for uploading songs -->
    <script src="js/uploader.js"></script>
</head>
<body>


    <div class="fixed-nav-bar">
    </div>
      <div class="example">
          
        <div class="player">
        <div style="position:relative;text-align:center;top:130px;color:#FFF;font-weight:bold;padding:10px" class="currTime"></div>
            <div class="artist"></div>
            <div class="controls">
                <div class="play"></div>
                <div class="pause"></div>
                <div class="rew"></div>
                <div class="fwd"></div>
            </div>

            <div class="volume"></div>
            <div class="tracker"></div>
        </div>


<ul class="playlist hidden">
    <?=$player->getAllsongs()?>
</ul>

</div>

<br>
<br>

<center>
<!-- Upload file -->
<form name="audio_form" id="audio_form" action="" method="post" enctype="multipart/form-data">

  <input type="file" name="audio_file" id="audio_file" accept="audio/mp3,audio/*;capture=microphone"/>
  <br> <br>
  <input class="btn" id="hide-btn" type="button" value="Upload naar playlist" onclick="uploadFile()">
  <br>

</form>

<br>
<br>
<!-- Clear playlist -->
<form name="empty-playlist" method="POST" action="delete.php">
<input class="fixed-nav-bar-bottom" type="submit" value="Playlist leegmaken">
</form>
</center>

<br>
</body>
</html>