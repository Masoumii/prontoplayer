<?php   
        // Remove all records of music files from database
        require "dbconfig.php";
        $conn = DatabaseConnection::getConnection();
        $q    = "TRUNCATE TABLE mp3";
        $stmt = $conn->prepare($q);
        $stmt->execute();

        // Remove all music files from server
        $files = glob('data/music/*'); // get all file names
        foreach($files as $file){ // iterate files
        if(is_file($file))
        unlink($file); // delete file
        }

        // Redirect back to home
        header("Location: index.php");
?>